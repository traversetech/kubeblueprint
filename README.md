# Kubernetes cluster blueprint with Ten64 and μVirt
![images/muvirt-portainer-side-by-side.png](images/muvirt-portainer-side-by-side.png)

This repository contains files for building a Kubernetes cluster using K3S/K3OS
and a Ten64.

The first part describes a 'self-contained' cluster where both the controller
and a number of node virtual machines reside on the one Ten64 box.

The second part describes how to build an 'edge' architecture where the Ten64
connects to a K3S controller elsewhere.

The demonstration applications for the stack are:

* [Portainer](https://www.portainer.io/)

    Portainer is a web-based management tool for Docker and Kubernetes. It helps
us view what nodes and applications are deployed in the cluster, as well
as useful features such as being able to get a console/shell prompt inside
a container/pod.

* Looking Glass

    Here were demonstrating in-cluster networking and service
discovery by providing a 'central' looking glass that can run
ping, traceroute and iperf3 from the viewpoint of a VM
that has been deployed in each cluster node.

![images/looking-glass-screenshot.png](images/looking-glass-screenshot.png)

No pre-existing knowledge of Kubernetes is assumed,
corrections and tips gratefully received. However, being familiar
with Docker is helpful, as well as a good understanding of *nix
and networking concepts.

To keep things simple, we do bend "best practice" rules
by deploying "user-facing" workloads on the k3s controller node,
in a production architecture you would normally deploy dedicated
nodes on the cloud side for this purpose.

## Self-contained tutorial
See the [self-contained](self-contained.md) file for the "self-contained"
tutorial which sets up a full, multi-node K3S cluster on a single Ten64.

## Edge tutorial
This tutorial extends the self-contained tutorial by deploying
a K3S controller in the "cloud", and shows you how to connect multiple
Ten64's to this controller over IPSec VPNs.

(TODO)