
## What you will need
* Ten64 with [μVirt](https://gitlab.com/traversetech/muvirt/) and a block storage device (e.g NVMe SSD)

* A client PC with [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

* An SSH key will let you access the controller and client nodes over the network
via SSH

## Setup μVirt for automated provisioning
See the [appliance store quickstart](https://gitlab.com/traversetech/muvirt/-/wikis/ApplianceStoreQuickStart)
for the setup process - at a minimum you need to run:

```
/usr/libexec/muvirt/muvirt-system-setup /dev/nvme0n1
```

This process is likely to be simplified by the time Ten64 production units
ship in Q1/CY2021 - stay tuned!

## Run the k3os wizard on your muvirt host
`k3os-cluster-wizard` will automate the creation of a k3s cluster with a controller
and multiple child nodes on the same host.

```
root@muvirt:~# k3os-cluster-wizard 
```
Select mode 1 - Build a self contained cluster:

```
K3OS self contained cluster wizard
Select mode: 
[1] - Build a self contained cluster
[2] - Create nodes to join an existing cluster
1
Going into self contained mode
```
The controller and worker VMs will be named `(basename)controller` and
`(basename)nodeX`, by default, `k3` will be used as the basename.

```
Enter the base name for this cluster, or [enter] for the default "k3"

For example: With a base name of k3, the nodes will be named: 
Controller: k3controller
Node 1: k3node1
Node 2: k3node2
Node X: k3nodeX

Cluster base name:
k3
Using "k3" as the cluster basename
```

At the moment, you need to enter a URL for the k3os ISO - soon we will
be able to use the appliance store to provide the latest version
```

Enter URL for k3os iso, or [enter] for the default https://github.com/rancher/k3os/releases/download/v0.11.1/k3os-arm64.iso

https://my.server/k3os-arm64.iso
```

You can create DHCP reservations for the controller and worker nodes, just to keep things tidy.
By default, the controller node will be numbered as .10 on your LAN subnet, and worker nodes starting from .11:

```
Create DHCP reservation for Master node? [y/n]
y
Downloading k3os iso from https://my.server/k3os-arm64.iso
Downloaded image, with SHA256SUM f83703001782295b097ced9663117ca9271979c29181ca6af67172bbb17021cd
```

To be able to login to the controller and worker nodes, a password is needed for the `rancher` user

As password logins are disabled over SSH, you should also specify an SSH key.

```
Enter password for k3os rancher user: 
hunter2

Enter SSH keys to allow for the rancher user, one per line, leave blank to finish
ssh-ed25519 ......
[hit enter once you have specified all the ssh keys]

Got 1 keys, continuing
```

muvirt will then proceed to build the controller VM. It will then wait for k3s to create
the cluster token, which is used by the worker nodes to authenticate to the controller.

```
Provisioning the master controller VM
Master node provisioning done, waiting for the cluster token to be created
Got cluster token: K10a0553d97b7c4bdee1282bfc7b896772fa43780970eeca5a3212f33a221aa21a8::server:b5533a1d75014997cfbf2a8f2ce24591
```

Once the controller is ready, we can create a couple of worker nodes:
```
Master node operational, create client nodes [y/n]?
y

How many client nodes should we create? 
4
Create DHCP Reservations for child nodes?
y
Provisioning node1
Provisioning node 1 done
Provisioning node2
Provisioning node 2 done
Provisioning node3
Provisioning node 3 done
Provisioning node4
Provisioning node 4 done
```

Once the worker nodes are provisioned, you will see the entire stack in `muvirt-status`:
```
root@muvirt:~# muvirt-status 
VM Name             |Status            |PID   |Network
--------------------|------------------|------|------------------------------
examplevm           |disabled          |      |
k3controller        |on                |9086  |eth0: 192.168.9.10/24 fd7c:9e0e:f356:0:5054:ff:feaa:7a34/64 flannel.1: 10.42.0.0/32 cni0: 10.42.0.1/24 
k3node1             |on                |9821  |eth0: 192.168.9.11/24 fd7c:9e0e:f356::11/64 fd7c:9e0e:f356:0:5054:ff:feee:6a50/64 flannel.1: 10.42.1.0/32 cni0: 10.42.1.1/24 
k3node2             |on                |10509 |eth0: 192.168.9.12/24 fd7c:9e0e:f356:0:5054:ff:fe7d:fc61/64 flannel.1: 10.42.2.0/32 cni0: 10.42.2.1/24 
k3node3             |on                |11185 |eth0: 192.168.9.13/24 fd7c:9e0e:f356::13/64 fd7c:9e0e:f356:0:5054:ff:feb4:2667/64 flannel.1: 10.42.3.0/32 cni0: 10.42.3.1/24 
k3node4             |on                |11878 |eth0: 192.168.9.14/24 fd7c:9e0e:f356:0:5054:ff:fe43:b878/64 flannel.1: 10.42.4.0/32 cni0: 10.42.4.1/24 
```
As you can see, the nodes have joined the cluster and are using flannel to communicate between each other. Pretty isn't it?

This is what it looks like in the web interface:

![images/muvirt-web-k3nodes.png](images/muvirt-web-k3nodes.png)

## Log into the controller node
Use `muvirt-console` or the LuCI interface to log into the controller node, and check
the output of `kubectl get node`.

It should look like this:

![images/kubectl-nodes.png](images/kubectl-nodes.png)

## Copy the kubectl config file from controller node to your client PC

Copy the `/etc/rancher/k3s/k3s.yaml` to `${HOME}/.kube/config`. 

You can also copy k3s.yaml to another location and use the `KUBECONFIG` environment
variable to reference it.

### Change the URL in the k3s configuration

The kubectl config from controller node is set up to access itself, with a URL
of "https://127.0.0.1:6443". We need to change it to use the local LAN IP of the
controller:

Change the `server` value under `clusters.cluster.server`:

From:
```
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ....
    server: https://127.0.0.1:6443
```

to:
```
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ....
    server: https://192.168.7.10:6443
```

Then run `kubectl get node`:
```
kubectl get node
NAME           STATUS   ROLES    AGE   VERSION
k3controller   Ready    master   51m   v1.19.2+k3s1
k3node3        Ready    <none>   30m   v1.19.2+k3s1
k3node4        Ready    <none>   29m   v1.19.2+k3s1
k3node5        Ready    <none>   28m   v1.19.2+k3s1
k3node1        Ready    <none>   31m   v1.19.2+k3s1
k3node2        Ready    <none>   31m   v1.19.2+k3s1
```

### Deploy Portainer

Create a portainer namespace;
```
kubectl namespace create portainer
```

Deploy portainer using the manifest:
```
kubectl apply --namespace=portainer -f portainer.yaml
```

Check to see that the portainer pod is up:
```
kubectl --namespace=portainer get pod
NAME                        READY   STATUS    RESTARTS   AGE
portainer-9d8c54f6b-gpm9j   1/1     Running   0          11m
```

Once portainer is ready, you can go to "http://controller:30777" to do the
portainer setup

### Deploy Looking Glass
#### Edge nodes

##### SSH Keypair generation
First, generate an SSH key which the looking glass application will use to
access the "edge" nodes.

This key must be RSA due to limitations in the SSH library the looking glass
application uses:

```
ssh-keygen -t rsa -f lgobserver_key
```

Do not assign a passphrase for the keypair (leave it blank)

##### Import SSH keypair
Private key:

```
kubectl --namespace=lookingglass create secret generic lgcontainer-ssh-key --from-file=id_rsa=lgobserver_key
```

Public key:
```
kubectl --namespace=lookingglass create secret generic lgcontainer-ssh-pubkeys --from-file=authorized_keys=lgobserver_key.pub
```

##### Deploy edge nodes

```
kubectl --namespace="lookingglass" apply -f looking-glass/lgendpoints.yaml
```

#### Deploy iperf3 endpoint on master node
```
kubectl --namespace="lookingglass" apply -f looking-glass/iperf3.yaml
```

#### Deploy Looking Glass Website

If your controller node is ARM64 based:
```
kubectl --namespace="lookingglass" apply -f looking-glass/lgweb.yaml
```

If your controller node is x86 based (as it might be for cloud/edge usecases), use `lgweb-x86.yaml` instead.

The looking glass will now be available at "http://MASTERNODE:30090/".

You should see something like this:
![images/looking-glass-screenshot.png](images/looking-glass-screenshot.png)

## Links to the container sources/repo
* [Kubernetes Looking Glass](https://gitlab.com/matt_traverse/kubernetes-looking-glass)

    This is a fork/modification of the [respawner looking-glass](https://github.com/respawner/looking-glass),
modified for this particular use case (e.g no bgp, automated discovery from CoreDNS, iperf3 function)

* [Looking Glass Sensor container](https://gitlab.com/matt_traverse/lg-sensor-container)

## Appendix: adding docker registry credentials to Kubernetes
While all of the containers in this repository are publicly available,
for development, I used my own private gitlab server, which meant
I had to supply docker registry credentails.

You can import your existing "${HOME}/docker/config.json", or login directly
with kubectl:

```
kubectl --namespace=lookingglass create secret docker-registry lgcredentialdeploy \
    --docker-server=<your-registry-server> \
    --docker-username=<your-name> \
    --docker-password=<your-pword> --docker-email=<your-email>
```

See the document kubernetes.io document ["Pull an Image from a Private Registry"](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)
for more information

In the manifest files, you will need to specify `imagePullSecrets` alongside the container:
```
...
      containers:
        - name: lookingglass
          image: registry.gitlab.com/matt_traverse/lgcontainer:arm64
          imagePullPolicy: Always
          volumeMounts:
            - name: sshkeydir
              mountPath: "/run/sshkeydir"
          ports:
            - containerPort: 22
      imagePullSecrets:
        - name: lgcontainerdeploy
```